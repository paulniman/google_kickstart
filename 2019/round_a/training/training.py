#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   April 07, 2020 - 20:41:15
# @Last Modified: April 07, 2020 - 22:55:02
#
################################################################################


def print_solution(case_num, solution):
    """Helper function for formatting the answer the way Kickstart wants it
    """
    print('Case #{}: {}'.format(case_num, solution))


def find_min_hours(n, p, players):
    """Finds the minimum number of hours required to form a complete team.

    The requirements for a team are that it has to have exactly p players,
    and their skill levels must all be equal. It requires 1 hour of
    training to add 1 point of skill to a single player.

    Algorithm breakdown:

    1. Sort the list of player skills
    2. Add the first p players to the "team"
    3. Calculate the training required for the first p players
    4. For each subsequent player in the pool:
        a. Diff the new player's skill with the best player on the team, multiply
           this value by p, add it to the team's required training
        b. Diff the skills of the new player the worst player on the team,
           subtract it from the team's required training
        c. See if the new training amount is lower than the previous lowest
    5. Return the lowest recorded amount of training
    """
    players.sort()
    team = {i: players[i] for i in range(p)} # Dictionary for faster lookups
    best_idx = p-1 # Track where in the dictionary the best player is
    min_hours = sum([team[best_idx] - team[i] for i in range(p)])
    hours = min_hours
    for i in range(p, n):
        hours += (p * (players[i] - team[best_idx]))
        best_idx = (best_idx+1) % p
        hours -= (players[i] - team[best_idx])
        team[best_idx] = players[i]
        if hours < min_hours:
            min_hours = hours

    return min_hours


def main():
    t = int(input()) # Number of test cases
    solutions = []
    for i in range(t):
        # Read the test case data from stdin
        n, p = [int(x) for x in input().split()]
        players = sorted([int(x) for x in input().split()])
        solutions.append(find_min_hours(n, p, players))

    for i in range(len(solutions)):
        print_solution(i+1, solutions[i])


if __name__ == '__main__':
    main()
