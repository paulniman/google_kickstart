# My Google Kickstart Solutions

This repository contains my solutions to Google Kickstart problems. As of now,
they are all written in Python3.

Each different problem is contained within a unique folder. The folder structure
is as follows:

YEAR1
    Round A
        Problem 1
        Problem 2
        Problem 3
    Round B
        Problem 1
        Problem 2
    ...
YEAR2
...

